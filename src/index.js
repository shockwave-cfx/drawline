let enabled = false;
let recordings = [];

setTick(() => {
  if (
    IsControlJustPressed(0, INPUT_VEH_HORN) &&
    IsPedInAnyVehicle(PlayerPedId(), false)
  ) {
    enabled = !enabled;

    if (enabled) {
      recordings.push({ points: [] });
    }
  }

  recordings.forEach((recording) => {
    recording.points.forEach((to, index) => {
      if (index > 0) {
        const from = recording.points[index - 1];
        const [x1, y1, z1] = from.pos;
        const [x2, y2, z2] = to.pos;
        const [r, g, b] = from.onGround
          ? [255, 255, 255]
          : [255, 0, 0];

        DrawLine(x1, y1, z1, x2, y2, z2, r, g, b, 255);
      }
    });
  });
});

setInterval(() => {
  if (!enabled) {
    return;
  }

  if (!IsPedInAnyVehicle(PlayerPedId(), false)) {
    enabled = false;
    return;
  }

  const { points } = recordings[recordings.length - 1];
  const vehicle = GetVehiclePedIsIn(PlayerPedId(), false);
  const [posX, posY, posZ] = GetEntityCoords(vehicle);
  const [rotX, rotY, rotZ] = GetEntityRotation(vehicle);

  if (points.length) {
    const [offX, offY, offZ] = points[points.length - 1].pos;
    const distance = GetDistanceBetweenCoords(posX, posY, posZ, offX, offY, offZ, true);

    if (distance < 1) {
      return;
    }
  }

  points.push({
    onGround: GetEntityHeightAboveGround(vehicle) < 1,
    pos: [posX, posY, posZ],
    rot: [rotX, rotY, rotZ]
  });
}, 100);
